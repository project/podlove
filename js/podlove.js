/**
 * @file
 * Lorem ipsum dolor sit amet.
 **/
(function (Drupal, drupalSettings, once) {
  function init(player) {
    var podloveFileId = player.dataset.podloveFileId
    var episode = drupalSettings.podlove[podloveFileId]['episode']
    var config = drupalSettings.podlove[podloveFileId]['config']

    window.podlovePlayer(
      `[data-podlove-file-id="${podloveFileId}"]`,
      episode,
      config
    )
  }

  Drupal.behaviors.podlove = {
    attach: function attach(context) {
      once('podlove', '.podlove-web-player', context).forEach(init);
    }
  };
})(Drupal, drupalSettings, once);
