<?php

namespace Drupal\podlove;

/**
 * Interface for podlove_client plugins.
 */
interface PodloveClientInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * @return mixed[]
   *   The element's form array.
   */
  public function settingsForm();

  /**
   * @return bool
   *   TRUE if client has custom service id.
   */
  public function usesCustomServiceId();

  /**
   * @return mixed[]
   *   The config array.
   */
  public function getClientSettings();

}
