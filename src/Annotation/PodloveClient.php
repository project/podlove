<?php

namespace Drupal\podlove\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines podlove_client annotation object.
 *
 * @Annotation
 */
class PodloveClient extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * TRUE if client needs a custom service id.
   *
   * @var bool
   */
  public $uses_custom_service_id;

}
