<?php

namespace Drupal\podlove;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\podcast_publisher\PodcastEpisodeInterface;

/**
 * Service to generate settings for podlove web player.
 */
class PodloveSettings {

  /**
   * The podlove client plugin manager.
   *
   * @var \Drupal\podlove\PodloveClientPluginManager
   */
  protected $podloveClientPluginManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The podlove client plugin manager.
   *
   * @param \Drupal\podlove\PodloveClientPluginManager $manager
   *   The podlove client plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(PodloveClientPluginManager $manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->podloveClientPluginManager = $manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Extracts the episode info of given podcast episode.
   *
   * @param \Drupal\podcast_publisher\PodcastEpisodeInterface $podcast_episode
   *   The podacast episode media entity.
   *
   * @return mixed[]
   *   Associative array containing the episode info.
   */
  public function generateEpisodeInformation(PodcastEpisodeInterface $podcast_episode) {
    /** @var \Drupal\podcast_publisher\PodcastInterface $podcast */
    $podcast = $podcast_episode->podcast->entity;
    if (!$podcast) {
      return [];
    }
    $publication_date = DrupalDateTime::createFromTimestamp($podcast_episode->created->value);
    /** @var \Drupal\image\Entity\ImageStyle $image_style */
    $image_style = $this->entityTypeManager->getStorage('image_style')->load('podcast_3000x3000');

    $podcast_image = NULL;
    if ($image_file = $podcast->image->entity) {
      $podcast_image = $image_style->buildUrl($image_file->uri->value);
    }

    $episode_image = NULL;
    if ($image_file = $podcast_episode->episode_image->entity) {
      $episode_image = $image_style->buildUrl($image_file->uri->value);
    }

    /** @var \Drupal\file\FileInterface $file */
    $file = $podcast_episode->audio_file->entity;
    $config = [
      'version' => 5,
      'show' => [
        'title' => $podcast->getTitle(),
        'subtitle' => $podcast->subtitle->value,
        'summary' => $podcast->summary->value,
        'poster' => $podcast_image,
        'link' => 'https://podlovers.org/podlove-web-player',
      ],
      'title' => $podcast_episode->label(),
      'summary' => $podcast_episode->shownotes->value,
      'publicationDate' => $publication_date->format(\DateTime::ISO8601),
      'poster' => $episode_image,
      'duration' => $podcast_episode->duration->value,
      'audio' => [
        [
          'url' => $file->createFileUrl(),
          'title' => $podcast_episode->label(),
          'size' => $file->getSize(),
          'mimeType' => $file->getMimeType(),
        ],
      ],
    ];
    return $config;
  }

  /**
   * Generates player config based on podcast episode.
   *
   * @param \Drupal\podcast_publisher\PodcastEpisodeInterface $podcast_episode
   *   The podacast episode media entity.
   *
   * @return mixed[]
   *   Associative array containing the player configuration.
   */
  public function generatePlayerConfig(PodcastEpisodeInterface $podcast_episode) {
    /** @var \Drupal\podcast_publisher\PodcastInterface $podcast */
    $podcast = $podcast_episode->podcast->entity;
    $url = Url::fromRoute('view.podcast_feed.podcast_feed', ['arg_0' => $podcast->id()]);
    $url->setAbsolute(TRUE);
    return [
      'version' => 5,
      'subscribe-button' => [
        'feed' => $url->toString(),
        'clients' => $this->getPodloveClients($podcast->field_podlove_client_settings->settings['clients']),
      ],
    ];
  }

  /**
   * Returns podlove client settings.
   *
   * @param mixed[] $field_data
   *   The field data.
   *
   * @return mixed[]
   *   The client settings.
   */
  public function getPodloveClients($field_data) {
    $clients = [];
    foreach ($field_data as $plugin_id => $data) {
      if ($data['value']) {
        /** @var \Drupal\podlove\PodloveClientInterface $plugin */
        $plugin = $this->podloveClientPluginManager->createInstance($plugin_id, isset($data['settings']) ? $data['settings'] : []);
        $clients[] = $plugin->getClientSettings();
      }
    }
    return $clients;
  }

}
