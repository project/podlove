<?php

namespace Drupal\podlove;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * PodloveClient plugin manager.
 */
class PodloveClientPluginManager extends DefaultPluginManager {

  /**
   * Constructs PodloveClientPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/PodloveClient',
      $namespaces,
      $module_handler,
      'Drupal\podlove\PodloveClientInterface',
      'Drupal\podlove\Annotation\PodloveClient'
    );
    $this->alterInfo('podlove_client_info');
    $this->setCacheBackend($cache_backend, 'podlove_client_plugins');
  }

}
