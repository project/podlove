<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "google-podcasts",
 *   label = @Translation("Google Podcasts"),
 *   uses_custom_service_id = TRUE
 * )
 */
class GooglePodcasts extends PodloveClientPluginBase {

}
