<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "beyond-pod",
 *   label = @Translation("Beyond Pod"),
 *   uses_custom_service_id = FALSE
 * )
 */
class BeyondPod extends PodloveClientPluginBase {

}
