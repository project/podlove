<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "instacast",
 *   label = @Translation("Instacast"),
 *   uses_custom_service_id = FALSE
 * )
 */
class Instacast extends PodloveClientPluginBase {

}
