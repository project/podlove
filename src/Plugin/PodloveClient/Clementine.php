<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "clementine",
 *   label = @Translation("Clementine"),
 *   uses_custom_service_id = FALSE
 * )
 */
class Clementine extends PodloveClientPluginBase {

}
