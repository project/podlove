<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "rss-radio",
 *   label = @Translation("RSS Radio"),
 *   uses_custom_service_id = FALSE
 * )
 */
class RssRadio extends PodloveClientPluginBase {

}
