<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "pod-grasp",
 *   label = @Translation("PodGrasp"),
 *   uses_custom_service_id = FALSE
 * )
 */
class PodGrasp extends PodloveClientPluginBase {

}
