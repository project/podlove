<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "podcat",
 *   label = @Translation("Podcat"),
 *   uses_custom_service_id = FALSE
 * )
 */
class Podcat extends PodloveClientPluginBase {

}
