<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "overcast",
 *   label = @Translation("Overcast"),
 *   uses_custom_service_id = FALSE
 * )
 */
class Overcast extends PodloveClientPluginBase {

}
