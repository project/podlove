<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "castro",
 *   label = @Translation("Castro"),
 *   uses_custom_service_id = FALSE
 * )
 */
class Castro extends PodloveClientPluginBase {

}
