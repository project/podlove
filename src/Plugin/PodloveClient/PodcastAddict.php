<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "podcast-addict",
 *   label = @Translation("Podcast Addict"),
 *   uses_custom_service_id = FALSE
 * )
 */
class PodcastAddict extends PodloveClientPluginBase {

}
