<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "gpodder",
 *   label = @Translation("gPodder"),
 *   uses_custom_service_id = FALSE
 * )
 */
class GPodder extends PodloveClientPluginBase {

}
