<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "apple-podcasts",
 *   label = @Translation("Apple Podcasts"),
 *   uses_custom_service_id = TRUE
 * )
 */
class ApplePodcasts extends PodloveClientPluginBase {

}
