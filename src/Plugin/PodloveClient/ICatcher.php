<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "i-catcher",
 *   label = @Translation("iCatcher"),
 *   uses_custom_service_id = FALSE
 * )
 */
class ICatcher extends PodloveClientPluginBase {

}
