<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "podcast-republic",
 *   label = @Translation("Podcast Republic"),
 *   uses_custom_service_id = FALSE
 * )
 */
class PodcastRepublic extends PodloveClientPluginBase {

}
