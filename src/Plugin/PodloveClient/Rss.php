<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "rss",
 *   label = @Translation("RSS"),
 *   description = @Translation("RSS feed."),
 *   uses_custom_service_id = FALSE
 * )
 */
class Rss extends PodloveClientPluginBase {

}
