<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "downcast",
 *   label = @Translation("Downcast"),
 *   uses_custom_service_id = FALSE
 * )
 */
class Downcast extends PodloveClientPluginBase {

}
