<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "antenna-pod",
 *   label = @Translation("Antenna Pod"),
 *   uses_custom_service_id = FALSE
 * )
 */
class AntennaPod extends PodloveClientPluginBase {

}
