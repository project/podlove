<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "pocker-casts",
 *   label = @Translation("PocketCasts"),
 *   uses_custom_service_id = FALSE
 * )
 */
class PocketCasts extends PodloveClientPluginBase {

}
