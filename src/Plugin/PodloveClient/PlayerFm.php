<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "player-fm",
 *   label = @Translation("Player FM"),
 *   uses_custom_service_id = FALSE
 * )
 */
class PlayerFm extends PodloveClientPluginBase {

}
