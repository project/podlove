<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "spotify",
 *   label = @Translation("Spotify"),
 *   uses_custom_service_id = TRUE
 * )
 */
class Spotify extends PodloveClientPluginBase {

}
