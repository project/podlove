<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "podscout",
 *   label = @Translation("Podscout"),
 *   uses_custom_service_id = FALSE
 * )
 */
class Podscout extends PodloveClientPluginBase {

}
