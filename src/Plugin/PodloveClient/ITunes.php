<?php

namespace Drupal\podlove\Plugin\PodloveClient;

use Drupal\podlove\PodloveClientPluginBase;

/**
 * Plugin implementation of the podlove_client.
 *
 * @PodloveClient(
 *   id = "itunes",
 *   label = @Translation("iTunes"),
 *   uses_custom_service_id = FALSE
 * )
 */
class ITunes extends PodloveClientPluginBase {

}
