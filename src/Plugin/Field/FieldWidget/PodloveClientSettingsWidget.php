<?php

namespace Drupal\podlove\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'podlove_client_settings' widget.
 *
 * @FieldWidget(
 *   id = "podlove_client_settings_widget",
 *   label = @Translation("Podlove Client Settings Widget"),
 *   field_types = {
 *     "podlove_client_settings"
 *   }
 * )
 */
class PodloveClientSettingsWidget extends WidgetBase {

  /**
   * Podlove client plugin manager.
   *
   * @var \Drupal\podlove\PodloveClientPluginManager
   */
  protected $podloveClientPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings']);
    $instance->podloveClientPluginManager = $container->get('plugin.manager.podlove_client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $plugin_definitions = $this->podloveClientPluginManager->getDefinitions();
    $settings = isset($items->getValue()[0]['settings']) ? $items->getValue()[0]['settings']['clients'] : [];
    $clients = [];
    foreach ($plugin_definitions as $plugin_id => $plugin_definition) {
      /** @var \Drupal\podlove\PodloveClientInterface $plugin */
      $plugin_settings = [];
      if (isset($settings[$plugin_id]) && $settings[$plugin_id]['value'] && isset($settings[$plugin_id]['settings'])) {
        $plugin_settings = $settings[$plugin_id]['settings'];
      }
      $plugin = $this->podloveClientPluginManager->createInstance($plugin_id, $plugin_settings);
      $clients[$plugin_id]['value'] = [
        '#type' => 'checkbox',
        '#title' => $plugin->label(),
        '#default_value' => isset($settings[$plugin_id]) ? $settings[$plugin_id]['value'] : 0,
      ];
      if ($plugin->usesCustomServiceId()) {
        $field_name = $items->getFieldDefinition()->getName();
        $input_name = $field_name . '[' . $delta . '][clients][' . $plugin_id . '][value]';
        $clients[$plugin_id]['settings'] = $plugin->settingsForm();
        $clients[$plugin_id]['settings']['#states'] = [
          'visible' => [
            'input[name="' . $input_name . '"]' => ['checked' => TRUE],
          ],
        ];
      }
    }
    $element['clients'] = array_merge([
      '#type' => 'fieldset',
      '#label' => $this->t('Podlove Clients'),
    ], $clients);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    return [
      [
        'settings' => $values[0],
      ],
    ];
  }

}
