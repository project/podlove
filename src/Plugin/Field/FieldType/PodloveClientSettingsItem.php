<?php

namespace Drupal\podlove\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Plugin implementation of the 'block_field' field type.
 *
 * @FieldType(
 *   id = "podlove_client_settings",
 *   label = @Translation("Podlove client settings"),
 *   description = @Translation("Stores the podlove client settings."),
 *   category = "general",
 *   default_widget = "podlove_client_settings_widget",
 *   cardinality = 1,
 *   module = "podlove",
 * )
 */
class PodloveClientSettingsItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['settings'] = MapDataDefinition::create()
      ->setLabel(t('Settings'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'settings' => [
          'description' => 'Serialized array of settings for the block.',
          'type' => 'blob',
          'size' => 'normal',
          'serialize' => TRUE,
        ],
      ],
    ];
  }

}
