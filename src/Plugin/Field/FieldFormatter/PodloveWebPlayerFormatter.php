<?php

namespace Drupal\podlove\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Podlove Web Player' formatter.
 *
 * @FieldFormatter(
 *   id = "podlove_web_player",
 *   label = @Translation("Podlove Web Player"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class PodloveWebPlayerFormatter extends FormatterBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Podlove settings service.
   *
   * @var \Drupal\podlove\PodloveSettings
   */
  protected $podloveSettings;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['label'], $configuration['view_mode'], $configuration['third_party_settings']);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->podloveSettings = $container->get('podlove.settings');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    foreach ($items as $delta => $item) {
      /** @var \Drupal\podcast_publisher\PodcastEpisodeInterface $podcast_episode */
      $podcast_episode = $items->getParent()->getEntity();
      $file_id = $item->entity->id();
      $template = <<<TWIG
<div class="podlove-web-player" data-podlove-file-id="{{ file_id }}"></div>
TWIG;

      $element[$delta] = [
        '#type' => 'inline_template',
        '#template' => $template,
        '#context' => [
          'file_id' => $file_id,
        ],
        '#attached' => [
          'library' => [
            'podlove/podlove',
          ],
          'drupalSettings' => [
            'podlove' => [
              $file_id => [
                'episode' => $this->podloveSettings->generateEpisodeInformation($podcast_episode),
                'config' => $this->podloveSettings->generatePlayerConfig($podcast_episode),
              ],
            ],
          ],
        ],
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getTargetEntityTypeId() == 'podcast_episode'
      && $field_definition->getName() == 'audio_file';
  }

}
