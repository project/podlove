<?php

namespace Drupal\podlove;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base class for podlove_client plugins.
 */
abstract class PodloveClientPluginBase extends PluginBase implements PodloveClientInterface {

  use StringTranslationTrait;

  protected $serviceId = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    if (isset($configuration['service_id'])) {
      $this->serviceId = $configuration['service_id'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm() {
    if ($this->pluginDefinition['uses_custom_service_id']) {
      return [
        '#type' => 'fieldgroup',
        'service_id' => [
          '#type' => 'textfield',
          '#title' => $this->t('Service ID'),
          '#default_value' => $this->serviceId,
        ],
      ];
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function usesCustomServiceId() {
    return $this->pluginDefinition['uses_custom_service_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getClientSettings() {
    $data = ['id' => $this->getPluginId()];
    if ($this->usesCustomServiceId()) {
      $data['service'] = $this->serviceId;
    }
    return $data;
  }

}
